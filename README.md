# Product Percentage Markup

## Overview

Product Percentage Markup is an Odoo module that provides a convenient way for users to increase product prices by a specified percentage using a wizard. This addon is designed to simplify the price management process within Odoo.

## Features

- Easy price adjustment: Users can easily adjust product prices by entering a percentage increase.
- Wizard Interface: The addon includes a user-friendly wizard interface for entering the percentage increase.
- Seamless Integration: Product Percentage Markup seamlessly integrates with Odoo's product management system.

## Installation

1. Make sure you have Odoo 16 Community Edition installed.

2. Clone or download this repository.

3. Place the 'product_percentage_markup' folder in your Odoo addons directory.

4. Restart your Odoo server.

5. Go to the Odoo Apps menu and activate the 'Product Percentage Markup' module.

## Usage

1. After activating the module, navigate to the 'Products' section in Odoo.

2. Select the products you want to adjust the prices for.

3. Click on the 'More' button and choose 'Increase Prices' from the dropdown menu.

4. A wizard will appear, allowing you to enter the percentage increase for the selected products.

5. After entering the percentage, click 'Apply Increase.'

6. The prices of the selected products will be updated accordingly.

## License

This addon is licensed under the GNU General Public License Version 2. You can find the full license text in the [LICENSE.md](LICENSE.md) file.

## Author

- Name: Alitux

## Repository

You can find the source code for this module on GitLab: [Product Percentage Markup Repository](https://gitlab.com/alitux/product-percentage-markup)

## Support

If you encounter any issues or have questions, please contact me. :)

## Contributing

1. Fork the repository.
2. Create a new branch for your feature or bug fix.
3. Make your changes and submit a pull request.

