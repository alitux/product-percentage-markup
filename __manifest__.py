# -*- coding: utf-8 -*-
{
    'name': "Product Percentage Markup",

    'summary': """
        The purpose of this addon is to enable Odoo users to 
        easily increase product prices by a specific percentage through a wizard.""",

    'description': """
        The purpose of this addon is to enable Odoo users to 
        easily increase product prices by a specific percentage through a wizard.""",

    'author': "Alitux",
    'website': "https:///delkarukinka.wordpress.com",
    'category': 'Sales',
    'version': '0.1',

    'depends': ['base', 'product', 'sale_management', 'account'],

    'data': [
        'security/ir.model.access.csv',
        'wizard/wizard_view.xml',
    ],
}
