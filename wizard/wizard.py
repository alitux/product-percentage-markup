from odoo import models, fields, api

class PriceIncreaseWizard(models.TransientModel):
    _name = 'price.increase.wizard'
    _description = 'Wizard Percentage Markup'

    markup_in = fields.Selection([
        ('cost', 'Cost'),
        ('list', 'List Price'),
    ], string='Markup In', required=True)
    percentage_increase = fields.Float(string='Percentage Amount', required=True)

    def apply_increase(self):
        products = self.env['product.template'].search([])
        if self.markup_in == 'list':
            for product in products:
                new_price = product.list_price * (1 + self.percentage_increase / 100)
                product.write({'list_price': new_price})
        if self.markup_in == 'cost':
            for product in products:
                new_price = product.standard_price * (1 + self.percentage_increase / 100)
                product.write({'standard_price': new_price})
